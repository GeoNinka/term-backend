from django.db import models

class Sale(models.Model):
    phone = models.CharField(max_length = 255, blank=False, unique=True)
    title = models.CharField(max_length = 255, blank=False)
    description = models.TextField()
    

    def __str__(self):
        return self.phone