"""django_api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from rest_framework import routers

from achievement.views import AchievementViewSet
from activity.views import ActivityViewSet
from contact_info.views import ContactInfoViewSet
from day.views import DayViewSet
from gallery.views import GalleryViewSet
from news.views import NewsViewSet


router = routers.DefaultRouter()
router.register(r'achievement', AchievementViewSet)
router.register(r'activity', ActivityViewSet)
router.register(r'contact-info', ContactInfoViewSet)
router.register(r'day', DayViewSet)
router.register(r'gallery', GalleryViewSet)
router.register(r'news', NewsViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls))
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)