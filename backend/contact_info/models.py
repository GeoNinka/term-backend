from django.db import models
from sale.models import Sale

class ContactInfo(models.Model):
    phone = models.ManyToManyField(Sale)
    email = models.EmailField(max_length=255, blank=False, unique=True)

    def __str__(self):
        return self.email