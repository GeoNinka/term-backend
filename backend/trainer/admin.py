from django.contrib import admin
from trainer.models import Trainer

from django.utils.safestring import mark_safe

class TrainerAdmin(admin.ModelAdmin):
    readonly_fields = ["preview"]

    def preview(self, obj):
        return mark_safe(f'<img src="{obj.image.url}" style="max-height: 300px;">')

admin.site.register(Trainer, TrainerAdmin)

