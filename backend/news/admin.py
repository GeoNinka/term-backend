from django.contrib import admin
from news.models import News

from django.utils.safestring import mark_safe

class NewsAdmin(admin.ModelAdmin):
    readonly_fields = ["preview"]

    def preview(self, obj):
        return mark_safe(f'<img src="{obj.image.url}" style="max-height: 300px;">')

admin.site.register(News, NewsAdmin)

